# URL

[http://www.dacu.com.ar/abaci-club/](http://www.dacu.com.ar/abaci-club/)

# Comandos

## La primera vez

```
npm install gulp -g
npm install
```

## Release ( para publicar / producción )

```
npm release
```

## Para desarrollo


### Para el sass watch
```
gulp

# Para cortar el watch en la consola apretar:

Control + C

```

### Debugger Js

en _dist/index.php linea 2 colocar:
```
$linksDebugger = true;
```