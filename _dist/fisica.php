<?php
$linksDebugger = true;

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC|Amaranth|Yanone+Kaffeesatz" rel="stylesheet">
	<link rel="stylesheet" href="css/app.min.css" type="text/css">
	<link rel="stylesheet" href="css/custom.css" type="text/css">

	<title>Abaci Club</title>
</head>

<body id="home">
	<?php include 'views/nav.php' ?>
	
	<div class="container">
		<div class="row">
			<?php include 'views/fisica/encabezado.html' ?>
		</div>
	</div>
		
	<?php include 'views/footer.html' ?>
				
	<!-- A continuación van los links a los Js, generados por gulp -->
	<?php
		if($linksDebugger)
		{			
			include 'views/linksDebugger.html';	
		}
		else
		{	
			include 'views/links.html';	
		}
		
?>
	
</body>

</html>