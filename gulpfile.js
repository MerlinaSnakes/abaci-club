var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var fileinclude = require('gulp-file-include');
var gulp = require('gulp');
var gutil = require('gulp-util');
var minify = require('gulp-minify');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var strip_comments = require('gulp-strip-json-comments');

var listJsDependencias = [
  'src/js/dependencias/jquery.js',
  'src/js/dependencias/bootstrap/transition.js',
  'src/js/dependencias/bootstrap/collapse.js',
  'src/js/dependencias/bootstrap/scrollspy.js',
  'src/js/dependencias/jquery.easing.min.js',
  'src/js/dependencias/jquery.magnific-popup.js',
  'src/js/dependencias/jquery.validate.min.js',
  'src/js/dependencias/owl.carousel.js'
];
var listJs = [
  'src/js/main.js'
];

gulp.task('jsDependencias', () => {
  return gulp
    .src(listJsDependencias)
    .pipe(concat('app.dependencias.js'))
    .pipe(minify({exclude: ['tasks']}))
    .pipe(gulp.dest('_dist/js'));
});

gulp.task('js', () => {
  return gulp
    .src(listJs)
    .pipe(concat('app.js'))
    .pipe(minify({exclude: ['tasks']}))
    .pipe(gulp.dest('_dist/js'));
});

gulp.task('sass', () => {
  return gulp
    .src('src/scss/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(concat('app.min.css'))
    .pipe(strip_comments())
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('_dist/css'))
});

gulp.task('linksJavascripts', () => {
  gulp
    .src('src/links.html')
    .pipe(fileinclude({
      context: {
        folder: '../',
        listJsDependencias: listJsDependencias,
        listJs: listJs
      }
    }))
    .pipe(concat('linksDebugger.html'))
    .pipe(gulp.dest('_dist/views/'));

  gulp
    .src('src/links.html')
    .pipe(fileinclude({
      context: {
        folder: '',
        listJsDependencias: ['js/app.dependencias-min.js'],
        listJs: ['js/app-min.js']
      }
    }))
    .pipe(gulp.dest('_dist/views/'))
});

var watchLogger = (event) => {
  gutil.log('[' + event.type + '] ' + event.path);
};

gulp.task('watch', ['release'], () => {
  var wSASS = gulp.watch('src/scss/**/*.scss', ['sass']);
  wSASS.on('change add unlink', watchLogger);
});

gulp.task('release', (cb) => {
  runSequence('linksJavascripts', 'sass', 'jsDependencias', 'js', cb);
});

gulp.task('default', ['watch']);